package ru.pgi.thread;

public class ThreadDemo extends Thread {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            (new ThreadDemo()).start();
        }
        System.out.println("Thread main started");
    }

    @Override
    public void run() {
        System.out.println("Thread run started " + getName());
    }
}

