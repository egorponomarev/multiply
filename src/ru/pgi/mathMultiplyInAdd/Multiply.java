package ru.pgi.mathMultiplyInAdd;
import java.util.Scanner;

/**
 * Класс реализует математческую операцию(умножение)
 *
 * @author Ponomarev G.I. 17IT18
 */

public class Multiply {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first operand = ");
        int firstNumber = scanner.nextInt();
        System.out.println("Enter second operand = ");
        int secondNumber = scanner.nextInt();
        Multiply.multiply(firstNumber, secondNumber);
    }

    /**
     * Проверка условий и вывод результата
     *
     * @param firstNumber  - множитель
     * @param secondNumber - множимое
     */
    public static int multiply(int firstNumber, int secondNumber) {

        if (firstNumber == 0 || secondNumber == 0) {
            System.out.println("Your result = 0! ");
        }
        int negNumb = 0;
        if (firstNumber < 0) {
            firstNumber *= -1;
            negNumb++;
        }
        if(secondNumber < 0){
            secondNumber *= -1;
            negNumb++;
        }
        int result = 0;
        boolean оdd = false;
        if ( secondNumber % 2 != 0) {
            secondNumber--;
            оdd = true;
        }
            if (firstNumber > secondNumber) {
                int value = firstNumber;
                firstNumber = secondNumber;
                secondNumber = value;
            }
            secondNumber /= 2;
            for (int i = 0; i < secondNumber; i++) {
                result += firstNumber + firstNumber;
            }
            if(оdd == true){
                result += firstNumber;
            }
            if(negNumb ==1){
                result *=-1;
            }

            System.out.println("Result mulplycation = " + result);

        return result;
    }
}
