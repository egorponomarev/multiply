package ru.pgi.mathMultiplyInAdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplyTest {

        @Test
        public void multiplyPositive() {
            int a = 1 + (int) (Math.random() * 100);
            int b = 1 + (int) (Math.random() * 100);
            assertEquals(a * b, Multiply.multiply(a, b));
        }

        @Test
        public void multiplyNegative() {
            int a = -100 + (int) (Math.random() * 99);
            int b = -100 + (int) (Math.random() * 99);
            assertEquals(a * b, Multiply.multiply(a, b));
        }

        @Test
        public void multiplyZeroByZero() {
            int a = 0;
            int b = 0;
            assertEquals(0, Multiply.multiply(a, b));
        }

        @Test
        public void multiplyNumByZero() {
            int a = 1 + (int) (Math.random() * 100);
            int b = 0;
            assertEquals(a * b, Multiply.multiply(a, b));
        }

        @Test
        public void multiplyZeroByNum() {
            int a = 0;
            int b = 1 + (int) (Math.random() * 100);
            assertEquals(a * b, Multiply.multiply(a, b));
        }
        @Test
        public void multiplyPozitiveByNegative() {
            int a = 1 + (int) (Math.random() * 100);
            int b = -100 + (int) (Math.random() * 99);
            assertEquals(a * b, Multiply.multiply(a, b));
        }

        @Test
        public void multiplyNegativeByPozitive(){
            int a = -100 + (int) (Math.random() * 99);
            int b = 1 + (int) (Math.random() * 100);
            assertEquals(a * b,Multiply.multiply(a,b));
        }
        @Test
        public void multiplyNegativeByZero(){
            int a = 0;
            int b = -100 + (int) (Math.random() * 99);
            assertEquals(a * b,Multiply.multiply(a,b));
        }
        @Test
        public void multiplyZeroByNegative(){
            int a = -100 + (int) (Math.random() * 99);
            int b = 0;
            assertEquals(a * b,Multiply.multiply(a,b));
        }
}
